import { useState, useEffect, FunctionComponent } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { menu } from "../data";
import { motion as m } from "framer-motion";

const NavItem: FunctionComponent<{
  active: string;
  setActive: Function;
  name: string;
  route: string;
}> = ({ active, setActive, name, route }) => {
  return (
    <Link href={route}>
      <a>
        {active !== name ? (
          <span
            style={{ width: "300px" }}
            className="transpan mx-2 cursor-pointer hover:border-b-4 hover:text-green"
            onClick={() => setActive(name)}
          >
            {name}
          </span>
        ) : (
          <span
            style={{ width: "300px" }}
            className="text-xl font-bold border-b-4 md:text-2xl border-green"
            onClick={() => setActive(name)}
          >
            {name}
          </span>
        )}
      </a>
    </Link>
  );
};

const Navbar = () => {
  const { pathname } = useRouter();

  const [active, setActive] = useState("");

  //later
  useEffect(() => {
    if (pathname === "/") setActive("About");
    else if (pathname === "/projects") setActive("Projects");
    else if (pathname === "/resume") setActive("Resume");
  }, []);

  return (
    <div className="flex flex-row content-center px-5 py-3 my-3 text-center">
      {menu.map((item) => (
      
          <Link href={item.route} className="w-auto">
              <div onClick={() => setActive(item.name)}
          className={`w-1/3 `} style={{cursor: 'pointer'}}
        >
            <a className="w-auto" >
              {/* <NavItem
              active={active}
              setActive={setActive}
              name={item.name}
              route={item.route}
            /> */}

              <span className={`text-xl font-bold md:text-2xl mr-auto ${item.name === active? 'text-gray-600 dark:text-green' :'text-green dark:text-gray-600'} `} >
             {item.name}
              </span>
              {/* <div className="underline"></div> */}
              {item.name === active ? (
                <m.div
                  className="underline bg-black dark:bg-green"
                  layoutId="underline"
                />
              ) : null}
            </a>
            </div>
          </Link>
       
      ))}
    </div>
  );
};

export default Navbar;
