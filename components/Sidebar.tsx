import { AiFillGithub, AiFillGitlab, AiFillLinkedin, AiFillYoutube } from "react-icons/ai";
import { GiTie } from "react-icons/gi";
import { GoLocation } from "react-icons/go";
import { useTheme } from "next-themes";
import Image from "next/image";
import { FunctionComponent, useEffect } from "react";
import { ItypeSidebar } from "../types";
import {motion as m} from "framer-motion";

const Sidebar: FunctionComponent<{types:ItypeSidebar}> = ({
  types:{type},
}) => {
  const { theme, setTheme } = useTheme();

 
  useEffect(() => {
    let ignore = false;
  
    
    if (!ignore) {
     
      
        setTheme("dark");
      
      
     
    }  
    return () => { ignore = true; }
    },[]);
 
  const changeTheme = () => {
    setTheme(theme === "light" ? "dark" : "light");
  };

  const testing = () =>{
    console.log("haha ke klick - ", theme);
    
  }
  


  return (
    <>
      <Image
        src="/images/foto_pp.jpg"
        alt="avatar"
        className=" mx-auto border rounded-full "
        height="128px"
        width="128px"
        layout="intrinsic"
        quality="100"
      />
      {/* <Image
        src="https://sumitdey.netlify.app/static/media/max.9d3a6d3e.jpg"
        alt="avatar"
        className=" mx-auto border rounded-full "
        height="128px"
        width="128px"
        layout="intrinsic"
        quality="100"
      /> */}
      <h3 className="my-4 text-3xl font-medium tracking-wider font-kaushan">
        <span className="text-white dark:text-green ">Masterio</span> Pemilutama
      </h3>
      <p className="px-2 py-1 my-3 bg-gray-200 rounded-full dark:bg-dark-200 dark:bg-black-500">
        Web Developer & Mobile Developer
      </p>
      {/* Resume */}
      <a
        href="https://drive.google.com/file/d/1bx_YLcGk_9rVBX5TtO_6oXX-gcB-7JwS/view?usp=sharing"
       target="_blank"
        className="flex items-center justify-center px-2 py-1 my-2 bg-gray-200 rounded-full cursor-pointer dark:bg-dark-200 dark:bg-black-500"
      >
        <GiTie className="w-6 h-6" />
        <span>Download Resume</span>
      </a>

      {/* Socials */}
      <div className="flex justify-around w-9/12 mx-auto my-5 md:w-full  dark:text-green">
        {/* <a href="https://www.youtube.com/channel/UClW8d1f5m0QAE_Ig024EP6A">
          <AiFillYoutube className="w-8 h-8 cursor-pointer" />
        </a> */}
        <a target="_blank" href="https://www.linkedin.com/in/masterio/" >
          <AiFillLinkedin className="w-8 h-8 cursor-pointer" />
        </a>
        <a target="_blank" href="https://gitlab.com/masterio12">
          <AiFillGitlab className="w-8 h-8 cursor-pointer" />{" "}
        </a>
      </div>

      {/* Contacts */}
      <div
        className="py-4 my-5 bg-gray-200 dark:bg-dark-200 dark:bg-black-500"
        style={{ marginLeft: "-1rem", marginRight: "-1rem" }}
      >
        <div className="flex items-center justify-center">
          <GoLocation className="mr-2" /> <span>Jakarta Selatan, Indonesia </span>
        </div>
        <p className={`my-2 emailProfile-up ${type == 'up'?'hidden':''}`} > masteriopemilutama@gmail.com</p>
        <p className={`my-2 emailProfile-down ${type == 'down'?'hidden':''}`} > masteriopemilutama@gmail.com</p>
        <p className="my-2 emailProfile-throw"> masteriopemilutama @gmail.com</p>
        <p className="my-2"> +62 857 9749 7316 </p>
      </div>

      {/* Email Button */}

      <button
        className="w-8/12 px-5 py-2 text-white  rounded-full cursor-pointer bg-green hover:scale-105 focus:outline-none"
        onClick={() => window.open("mailto:code.sumax@gmail.com")}
      >
        Email me
      </button>
      {/* <button
        onClick={changeTheme}
        className="w-8/12 px-5 py-2 my-4 text-white  rounded-full cursor-pointer bg-green focus:outline-none hover:scale-105 "
      >
     
        Toggle Theme
      </button> */}
      
      <m.div  className="flex items-center justify-center">
       
        
      <div onClick={changeTheme} className={` ${theme === "light" ? "toggle" : "toggle night"}`}>
        <div className="notch"></div>
        {/* cloud  */}
        <div className="shape tp"> </div>
        <div className="shape tp one"> </div>
        <div className="shape tp two"> </div>
       
        <div className="shape lf"> </div>
        <div className="shape lf one"> </div>
        <div className="shape lf two"> </div>

        <div className="shape rg"> </div>
        <div className="shape rg one"> </div>
        <div className="shape rg two"> </div>
       
        {/* <div className="shape md"></div> */}
        {/* <div className="shape lg"></div> */}
      </div>
      </m.div>
      
    </>
  );
};

export default Sidebar;
