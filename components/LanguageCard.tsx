import { FunctionComponent } from 'react'
import { Language, Service } from '../types'
// import { motion } from 'framer-motion'

const LanguageCard: FunctionComponent<{ language: Language }> = ({
   language: { Icon, name },
}) => {
   //XSS attack :( on our portfolio btw, as an alternate use npm i dom purify
   

   return (
      <div className='flex items-center p-2 space-x-4 '>
         <Icon className='w-3 h-3 text-green' />
         <div className=''>
            <h6 className='font-bold'>{name}</h6>
           
         </div>
      </div>
   )
}

export default LanguageCard
