import Bar from "../components/Bar";
import LanguageCard from "../components/LanguageCard";
import { languages, tools } from "../data";
import { motion as m } from "framer-motion";

const Resume = () => {
  return (
    <m.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.75 }}
    >
      <div className="px-6 py-2">
        {/* //! Education & Experience */}
        <div className="grid gap-6 md:grid-cols-2">
          <div>
            <h5 className="my-3 text-2xl font-bold">Education</h5>
            <div className="">
              <h5 className="my-2 text-xl font-bold">Dumbways.id</h5>
              <div className="grid  gap-6 md:grid-cols-2">
                <p className="font-semibold">Fullstack Developer</p>
                <a
                  target="_blank"
                  href="https://drive.google.com/file/d/1rdthvmChOGKRUHpMg1wknLbpmGVTtGIo/view?usp=sharing"
                >
                  <p className="font-semibold text-blue-500">
                    - certificate here
                  </p>{" "}
                </a>
              </div>

              <p className="my-3"></p>
            </div>
          </div>
          <div>
            <h5 className="my-3 text-2xl font-bold">Experience</h5>
            <div className="">
              <h5 className="my-2 text-xl font-bold">
                Front End & Mobile Developer
              </h5>
              <p className="font-semibold">
                in PT. Ayo Kreasi Teknologi (Mei 2021 - Present )
              </p>
              <p className="my-3">
                I am handle to build admin Web and mobile application for
                container and multipurpose ports
              </p>
            </div>
          </div>
        </div>

        {/*Languages & Tools */}
        <div className="grid gap-9 md:grid-cols-1">
          <div>
            <h5 className="my-3 text-2xl font-bold">Language & Framework</h5>
            <div className="my-2">
              {languages.map((val, i) => (
                // <Bar value={language} key={i} />
                <div className="col-span-2">
                  <LanguageCard language={val} />
                </div>

                // <h6 className='font-bold'> {val.name}</h6>
              ))}
            </div>
          </div>

          {/* <div>
          <h5 className="my-3 text-2xl font-bold">Tools & Softwares</h5>
          <div className="my-2">
            {tools.map((tool, i) => (
              <Bar value={tool} key={i} />
            ))}
          </div>
        </div> */}
        </div>
      </div>
    </m.div>
  );
};

export default Resume;
